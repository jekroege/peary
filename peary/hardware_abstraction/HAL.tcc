/**
 * Caribou HAL class implementation
 */
#ifndef CARIBOU_HAL_IMPL
#define CARIBOU_HAL_IMPL

#include "carboard/Carboard.hpp"

namespace caribou {

  template <typename T>
  std::shared_ptr<caribouHAL<T>>
  caribouHAL<T>::Factory(const std::string& hal_class, const std::string& device_path, uintptr_t device_address) {

    if(hal_class == "carboard") {
      return std::make_shared<carboard::Carboard<T>>(device_path, device_address);
    } else {
      throw caribouException("Unknown HAL class type " + hal_class);
    }
  }

  template <typename T>
  caribouHAL<T>::caribouHAL(std::string device_path, uintptr_t device_address)
      : _devpath(device_path), _devaddress(device_address), caribouHALbase(typeid(caribouHAL<T>)) {

    T& dev_iface = InterfaceManager::getInterface<T>(_devpath);
    LOG(DEBUG) << "Prepared HAL for accessing device with interface at " << dev_iface.devicePath();

    this->InitializeIfComplete();
  }

  template <typename T> caribouHAL<T>::~caribouHAL() { this->FinalizeIfComplete(); }

  template <typename T> void caribouHAL<T>::Initialize() {
    if(!caribou::caribouHALbase::generalResetDone) { // board needs to be reset
      generalReset();
    }
  }

  template <typename T> void caribouHAL<T>::Finalize() {}

  template <typename T> void caribouHAL<T>::generalReset() { caribou::caribouHALbase::generalResetDone = true; }

  template <typename T> void caribouHAL<T>::writeMemory(memory_map mem, uintptr_t value) { writeMemory(mem, 0, value); }

  template <typename T> void caribouHAL<T>::writeMemory(memory_map mem, size_t offset, uintptr_t value) {
    iface_mem& imem = InterfaceManager::getInterface<iface_mem>(MEM_PATH);
    imem.write(mem, std::make_pair(offset, value));
  }

  template <typename T> uintptr_t caribouHAL<T>::readMemory(memory_map mem) { return readMemory(mem, 0); }

  template <typename T> uintptr_t caribouHAL<T>::readMemory(memory_map mem, size_t offset) {
    iface_mem& imem = InterfaceManager::getInterface<iface_mem>(MEM_PATH);
    return imem.read(mem, offset, 1).front();
  }

  template <typename T> typename T::data_type caribouHAL<T>::send(const typename T::data_type& data) {
    return InterfaceManager::getInterface<T>(_devpath).write(_devaddress, data);
  }

  template <typename T>
  std::vector<typename T::data_type> caribouHAL<T>::send(const std::vector<typename T::data_type>& data) {
    return InterfaceManager::getInterface<T>(_devpath).write(_devaddress, data);
  }

  template <typename T>
  std::pair<typename T::reg_type, typename T::data_type>
  caribouHAL<T>::send(const std::pair<typename T::reg_type, typename T::data_type>& data) {
    return InterfaceManager::getInterface<T>(_devpath).write(_devaddress, data);
  }

  template <typename T>
  std::vector<typename T::data_type> caribouHAL<T>::send(const typename T::reg_type& reg,
                                                         const std::vector<typename T::data_type>& data) {
    return InterfaceManager::getInterface<T>(_devpath).write(_devaddress, reg, data);
  }

  template <typename T>
  std::vector<std::pair<typename T::reg_type, typename T::data_type>>
  caribouHAL<T>::send(const std::vector<std::pair<typename T::reg_type, typename T::data_type>>& data) {
    return InterfaceManager::getInterface<T>(_devpath).write(_devaddress, data);
  }

  template <typename T> std::vector<typename T::data_type> caribouHAL<T>::receive(const unsigned int length) {
    return InterfaceManager::getInterface<T>(_devpath).read(_devaddress, length);
  }

  template <typename T>
  std::vector<typename T::data_type> caribouHAL<T>::receive(const typename T::reg_type reg, const unsigned int length) {
    return InterfaceManager::getInterface<T>(_devpath).read(_devaddress, reg, length);
  }
} // namespace caribou

#endif
